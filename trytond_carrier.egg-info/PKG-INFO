Metadata-Version: 2.1
Name: trytond_carrier
Version: 7.0.1
Summary: Tryton module with carriers
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton carrier
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: Intended Audience :: Manufacturing
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: trytond_country<7.1,>=7.0
Requires-Dist: trytond_party<7.1,>=7.0
Requires-Dist: trytond_product<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0

Carrier Module
##############

The carrier module defines the concept of carrier.

Carrier
*******

A carrier links a party with a product and cost method.

- The *Product* is the carrier service.
- The *Carrier Cost Method* defines how to compute the carrier cost. By default
  there is only the *Product Price* method which takes the *List Price* of the
  *Product* as sale price and the *Cost Price* of the *Product* as purchase
  price.


Carrier Selection
*****************

A carrier selection defines the country of origin and destination where a
carrier can deliver the products.

- The *Sequence* is used to order the Carrier Selections.
- *Active* allows to disable a Carrier Selection.
- *From Country*  defines the Country of origin for this Carrier Selection.
  Empty value act as a wildcard.
- *To Country* defins the Country of destination for this Carrier Selection.
  Empty value act as a wildcard.
